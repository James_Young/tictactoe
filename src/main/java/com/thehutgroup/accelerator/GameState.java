package com.thehutgroup.accelerator;

import com.thehutgroup.accelerator.components.Piece;

public class GameState {


  private boolean win;
  private boolean draw;
  private Piece winner;

  public GameState(){

  }

  public boolean isWin(){
    return win;
  }

  public boolean isDraw(){
    return draw;
  }

  public boolean isOver(){
    return isWin() || isDraw();
  }

  public Piece getWinner() {
    return winner;
  }

  public Piece getLoser() {
    return Piece.getOther(getWinner());
  }
}
