package com.thehutgroup.accelerator;

import com.thehutgroup.accelerator.components.GameRunner;
import com.thehutgroup.accelerator.components.Piece;
import com.thehutgroup.accelerator.display.TicTacCLI;
import com.thehutgroup.accelerator.player.Computer;
import com.thehutgroup.accelerator.player.Human;
import com.thehutgroup.accelerator.player.Player;
import com.thehutgroup.accelerator.player.strategy.MoveStrategy;
import com.thehutgroup.accelerator.player.strategy.impl.EasyMoveStrategy;
import com.thehutgroup.accelerator.player.strategy.impl.HardMoveStrategy;
import com.thehutgroup.accelerator.player.strategy.impl.NormalMoveStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * <h1>Tic-Tac-Toe</h1>
 * An application to play tic-tac-toe
 *
 * @author James Young
 * @version 1.0
 * @since 2019-02-22
 */
public class Main {

  private static final Logger log = LoggerFactory.getLogger(Main.class);
  private static Scanner in = new Scanner(System.in);

  private static Player player1;
  private static Player player2;
  private static MoveStrategy moveStrategy;


  /**
   * This is the main application to take user input and run the game
   * @param args Unused
   */
  public static void main(String[] args) {

    log.info("Main program started");

    System.out.println("\nTIC TAC TOE\n");

    setUpGameOptions();

    GameRunner game = new TicTacCLI(player1, player2, 3);

    log.debug("About to start the game");
    game.startGame();

  }

  /**
   * Main method to set up the options for the game, including use of human or computer players, and difficulty settings
   */
  private static void setUpGameOptions(){
    int playerOption;
    while(true) {
      try {
        playerOption = requestPlayerOption();
        break;
      } catch (OptionException e) {
        System.out.println("Please select a valid option");
      }
    }

    MoveStrategy moveStrategy;
    switch (playerOption){
      case 1:
        player1 = new Human(Piece.X);
        player2 = new Human(Piece.O);
        break;
      case 2:
        player1 = new Human(Piece.X);
        moveStrategy = getMoveStrategyFromUser();
        player2 = new Computer(Piece.O, moveStrategy);
        break;
      case 3:
        moveStrategy = getMoveStrategyFromUser();
        player1 = new Computer(Piece.X, moveStrategy);
        player2 = new Computer(Piece.O, moveStrategy);
        break;
    }
  }

  /**
   * This method requests the move strategy from the user
   * @return MoveStrategy
   */
  private static MoveStrategy getMoveStrategyFromUser(){
    int difficultyOption;
    while (true) {
      try {
        difficultyOption = requestDifficultyOption();
        break;
      } catch (OptionException e) {
        System.out.println("Please select a valid option");
      }
    }
    switch (difficultyOption) {
      case 1:
        moveStrategy = new EasyMoveStrategy();
        break;
      case 2:
        moveStrategy = new NormalMoveStrategy();
        break;
      case 3:
        moveStrategy = new HardMoveStrategy();
        break;
    }
    return moveStrategy;
  }

  /**
   * Request player option from the user
   * @return integer representing the
   * @throws OptionException
   */
  private static int requestPlayerOption() throws OptionException {
    System.out.println("Select Player Option");
    List<String> options = new ArrayList<>();
    options.add("Human - Human");
    options.add("Human - Computer");
    options.add("Computer - Computer");
    displayOptions(options);
    return requestInteger(1, options.size());
  }

  private static int requestDifficultyOption() throws OptionException {
    System.out.println("Select Difficulty");
    List<String> options = new ArrayList<>();
    options.add("Easy");
    options.add("Normal");
    options.add("Hard");
    displayOptions(options);
    return requestInteger(1, options.size());
  }


  private static int requestInteger(int lowerLimit, int upperLimit) throws OptionException {
    try {
      System.out.print("> ");
      int inputInteger = in.nextInt();
      if (inputInteger < lowerLimit || inputInteger > upperLimit){
        throw new RuntimeException("Option " + inputInteger + " out of bounds (" + lowerLimit + ", " + upperLimit + ")");
      }
      return inputInteger;
    } catch(RuntimeException e) {
      throw new OptionException("Invalid option", e);
    } finally {
      in.nextLine();
    }
  }

  private static void displayOptions(List<String> options){
    border();
    for (int i=0; i<options.size(); i++){
      System.out.println(" (" + (i+1) + ") " + options.get(i));
    }
    border();
  }

  private static void border(){
    System.out.println("------------------------");
  }

}