package com.thehutgroup.accelerator.player;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.components.Piece;
import com.thehutgroup.accelerator.player.exceptions.MoveException;
import com.thehutgroup.accelerator.player.strategy.MoveStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Computer player class
 */

public class Computer extends Player {

  private static final Logger log = LoggerFactory.getLogger(Computer.class);

  private MoveStrategy strategy;

  /**
   * Constructor for the computer player class
   * @param piece defines the piece type assigned to the player
   * @param strategy defines the move strategy to be used by the computer player
   */
  public Computer(Piece piece, MoveStrategy strategy) {
    super(piece);
    setMoveStrategy(strategy);
    log.debug("Computer player defined, with piece {} and move strategy {}", piece, strategy);
  }

  /**
   * This method defines how the player is displayed in 'string' format
   * @return String
   */
  @Override
  public String toString() {
    return "Computer{" +
        "strategy=" + strategy +
        ", player=" + super.toString() + '}';
  }


  /**
   * Sets the move strategy to be used by the computer player
   * @param strategy defines the move strategy to be used by the computer player
   */
  private void setMoveStrategy(MoveStrategy strategy) {
    this.strategy = strategy;
  }

  /**
   * This method makes a move on the board, using the defined move strategy
   * @param board The board which the player is being asked to make a move on
   * @return Position The position where the player intends to move
   * @throws MoveException
   */
  @Override
  public Position makeMove(Board board) throws MoveException {
    return strategy.makeMove(board);
  }

}