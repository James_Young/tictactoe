package com.thehutgroup.accelerator.player.strategy.impl;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.player.exceptions.MoveException;
import com.thehutgroup.accelerator.player.strategy.MoveStrategy;

import java.util.List;
import java.util.Random;

/**
 * Easy move strategy class, which chooses a random move from the list of possible moves
 * @author James Young
 * @version 1.0
 * @since 2019-02-22
 */
public class EasyMoveStrategy implements MoveStrategy {

  private Random rand = new Random();

  /**
   * This method decides what move is to be made given the state of the board.
   * The implementation for the easy strategy is to choose a random move from the available positions
    * @param board defining the state of the board
   * @return Position where the strategy method decides to move
   * @throws MoveException if no move can be made
   */
  public Position makeMove(Board board) throws MoveException {
    List<Position> potentialMoves = board.getAvailablePositions();
    try {
      return potentialMoves.get(rand.nextInt(potentialMoves.size()));
    } catch (IllegalArgumentException e) {
      throw new MoveException("Board is full. Cannot make a move", e);
    }
  }

  /**
   * This method defines how the class is displayed as a string
   * @return String
   */
  @Override
  public String toString() {
    return "EasyMoveStrategy{}";
  }
}
