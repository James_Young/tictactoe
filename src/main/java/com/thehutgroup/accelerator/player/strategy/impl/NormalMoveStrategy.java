package com.thehutgroup.accelerator.player.strategy.impl;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.player.exceptions.MoveException;
import com.thehutgroup.accelerator.player.strategy.MoveStrategy;
import com.thehutgroup.accelerator.player.strategy.impl.EasyMoveStrategy;

import java.util.Random;


/**
 * Normal move strategy class, which currently defaults to the easy move strategy
 * @author James Young
 * @version 1.0
 * @since 2019-02-22
 */
public class NormalMoveStrategy implements MoveStrategy {
  private Random rand = new Random();

  /**
   * This method defines how the class is displayed as a string
   * @return String
   */
  @Override
  public String toString() {
    return "NormalMoveStrategy{}";
  }

  /**
   * This method defines how the class is displayed as a string
   * @return String
   */
  public Position makeMove(Board board) throws MoveException {
    MoveStrategy moveStrategy = new EasyMoveStrategy();
    return moveStrategy.makeMove(board);
  }
}
