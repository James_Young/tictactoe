package com.thehutgroup.accelerator.player.strategy;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.player.exceptions.MoveException;

/**
 * Interface for defining move strategies
 */
public interface MoveStrategy {
  /**
   * This method defines how the strategy makes a move given a board state
   * @param board defining the state of the board
   * @return Position where the strategy method decides to move
   * @throws MoveException
   */
  Position makeMove(Board board) throws MoveException;
}
