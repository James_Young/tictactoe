package com.thehutgroup.accelerator.player.strategy.impl;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.player.exceptions.MoveException;
import com.thehutgroup.accelerator.player.strategy.MoveStrategy;

import java.util.Random;

/**
 * Hard move strategy class, which currently defaults to the easy move strategy
 * @author James Young
 * @version 1.0
 * @since 2019-02-22
 */
public class HardMoveStrategy implements MoveStrategy {
  private Random rand = new Random();

  /**
   * This method defines how the class is displayed as a string
   * @return String
   */
  @Override
  public String toString() {
    return "HardMoveStrategy{}";
  }
  /**
   * This method decides what move is to be made given the state of the board.
   * The implementation currently defaults to the easy strategy
   * @param board defining the state of the board
   * @return Position where the strategy method decides to move
   * @throws MoveException if no move can be made
   */
  public Position makeMove(Board board) throws MoveException {
    MoveStrategy moveStrategy = new EasyMoveStrategy();
    return moveStrategy.makeMove(board);
  }
}
