package com.thehutgroup.accelerator.player;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.components.Piece;
import com.thehutgroup.accelerator.player.exceptions.MoveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <h1>Player</h1>
 * An abstract base class for players
 */
public abstract class Player {

  private Piece piece;

  private static final Logger log = LoggerFactory.getLogger(Player.class);

  /**
   * This is the constructor for player
   * @param piece This is the piece which is assigned to the player on creation
   */
  public Player(Piece piece) {
    this.piece = piece;
  }

  /**
   * @return Piece this returns the type of piece of the player
   */
  public Piece getPiece() {
    return piece;
  }

  /**
   * This method defines how the player is displayed in 'string' format
   * @return String
   */
  @Override
  public String toString() {
    return "Player{" +
        "piece=" + piece +
        '}';
  }

  /**
   * This is an abstract method to be defined in extensions of this class
   * @param board The board which the player is being asked to make a move on
   * @return Position The position where the player intends to move
   * @throws MoveException
   */
  abstract public Position makeMove(Board board) throws MoveException;
}
