package com.thehutgroup.accelerator.player.exceptions;

/**
 * Move exception class
 * This allows us to handle errors which occur when attempting to make moves
 */
public class MoveException extends Exception {

  /**
   * Handle the exception with a message
   * @param message summarizing why the exception was called
   */
  public MoveException(String message){
    super(message);
  }

  /**
   * Handle the exception with a message, and with the underlying cause of the exception
   * @param message summarizing why the exception was called
   * @param e the causing exception
   */
  public MoveException(String message, Exception e){
    super(message, e);
  }
}
