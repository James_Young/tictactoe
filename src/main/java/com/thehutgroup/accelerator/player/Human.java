package com.thehutgroup.accelerator.player;

import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.components.Piece;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.InputMismatchException;
import java.util.Optional;
import java.util.Scanner;

/**
 * Human player class, taking userinput from the console
 */
public class Human extends Player {

    private static final Logger log = LoggerFactory.getLogger(Human.class);

    private Scanner in = new Scanner(System.in);

    /**
     * Constructor for the human player class
     * @param piece Defines the piece type of the player
     */
    public Human(Piece piece){
        super(piece);
        log.debug("Human player created");
    }

    /**
     * This method defines how the player is displayed in 'string' format
     * @return String
     */
    @Override
    public String toString() {
        return "Human{" + super.toString() + "}";
    }

    //    @Override
//    public Position makeMove(board board) {
//
//        Position position;
//        while(true){
//            position = readMove();
//            if (!board.positionWithinBoard(position)){
//                System.out.println("Outside the range of the board! Try again.");
//            } else if (!board.isPositionVacant(position)){
//                System.out.println("That position is occupied! Try again.");
//            } else {
//                break;
//            }
//        }
//        return position;
//    }

    /**
     * This method makes a move on the board
     * @param board The board which the player is being asked to make a move on
     * @return Position The position where the player intends to move
     */
    @Override
    public Position makeMove(Board board) {

        Optional<Position> position;
        do {
            position = readMove();
        } while (!position.isPresent());

        return position.get();
    }

    /**
     * This method takes input from the console, requesting two ints specifying the row and column to make the move
     * @return Position where the player intends to move
     */
    private Optional<Position> readMove(){

        log.debug("Requesting move from the user");
        System.out.println("(row, col)");
        System.out.print("> ");
        try {
            int row = in.nextInt() - 1;
            int col = in.nextInt() - 1;
            return Optional.of(new Position(col, row));
        } catch (InputMismatchException e){
            log.warn("Invalid input for row, col");
            System.out.println("Integer input only please!");
            return Optional.empty();
        } finally {
            log.debug("Clearing the input stream");
            in.nextLine();
        }

    }
}
