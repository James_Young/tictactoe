package com.thehutgroup.accelerator.components;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;


/**
 * <h1>Board line class</h1>
 * Defines a board-line object used by the board checker class to check how many counters in a row each player has.
 * It iterates from a starting position on the board, and moves in the direction of a move vector to the next position
 */
public class BoardLine {

  private Position startPosition;
  private MoveVector moveVector;
  private Position currentPosition;
  private Board board;

  /**
   * Constructor to define the boardline object
   * @param startPosition starting position of the boardline
   * @param moveVector direction in which the iterate to from the current position
   * @param board the board object
   */
  public BoardLine(Position startPosition, MoveVector moveVector, Board board){
    this.startPosition = startPosition;
    this.moveVector = moveVector;
    this.board = board;
    currentPosition = startPosition;
  }

  /**
   * Gets the current position along the board line
   * @return currentPosition Position
   */
  public Position getCurrentPosition() {
    return currentPosition;
  }

  /**
   * Checks whether there are further available positions along the board line
   * @return boolean Returns true if the next possible position is within the boundaries of the board
   */
  public boolean hasNext(){
    Position nextPosition = getNextPosition();
    return board.positionWithinBoard(nextPosition);
  }

  /**
   * If the board line has a next position, this method will advance along the board line, and return the new position.
   * @return Position returns the next position
   */
  public Position next(){
    if (!hasNext()){
      throw new RuntimeException("Line has no next");
    }
    advanceAlongLine();
    return getCurrentPosition();
  }

  /**
   * Advances along the boardline by getting the next position, and setting that to the current position
   */
  private void advanceAlongLine(){
    Position nextPosition = getNextPosition();
    currentPosition = nextPosition;
  }

  /**
   * Gets the next position along the board line
   * @return Position The next position along the board line
   */
  private Position getNextPosition(){
    int col = currentPosition.getColumn();
    int row = currentPosition.getRow();
    col += moveVector.getCol();
    row += moveVector.getRow();
    return new Position(row, col);
  }



  public void reset(){
    currentPosition = startPosition;
  }
}
