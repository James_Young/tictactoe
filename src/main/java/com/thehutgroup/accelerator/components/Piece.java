package com.thehutgroup.accelerator.components;

/**
 * <h1>Tic Tac Piece</h1>
 * Defines the pieces used in tic-tac-toe as enum types
 */
public enum Piece {


  X("X"), O("O");

  private final String type;

  /**
   * Define the piece type string format
   * @param type String
   */
  Piece(String type) {
    this.type = type;
  }

  /**
   * Get the piece type in string format
   * @return String of the piece type
   */
  public String getType() {
    return type;
  }

  /**
   * Given a piece type, what is the other piece type?
   * @param piece Piece type
   * @return piece Piece type of the other piece
   */
  public static Piece getOther(Piece piece) {
    switch (piece) {
      case X:
        return O;
      case O:
        return X;
      default:
        throw new RuntimeException("Invalid piece type");
    }
  }

}
