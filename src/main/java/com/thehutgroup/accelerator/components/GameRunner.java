package com.thehutgroup.accelerator.components;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.player.Player;
import com.thehutgroup.accelerator.player.exceptions.MoveException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <h1>Game runner</h1>
 * Abstract class defining how the tic-tac game is played. Needs to be extended to define how the game should be displayed
 */
public abstract class GameRunner {

  private Player activePlayer;
  private Player passivePlayer;
  private Board board;
  private String state;
  private BoardChecker boardChecker;

  private static final Logger log = LoggerFactory.getLogger(Player.class);

  /**
   * Constructor for the game runner class
   * @param player1
   * @param player2
   * @param size
   */
  public GameRunner(Player player1, Player player2, int size){
    activePlayer = player1;
    passivePlayer = player2;
    board = new Board(size);
    state = "ready";
    boardChecker = new BoardChecker(size, board);
    log.debug("Gamerunner initialised with player1={} and player2={}", player1, player2);
  }

  /**
   * This method starts the game
   */
  public void startGame() {

    log.info("Game started");

    state = "play";

    while (!boardChecker.checkBoardForWinner(board) && !noMoreMoves()) {
      log.info("{}'s turn to play", activePlayer.getPiece());
      display();
      move();
      swapTurns();
    }
    display();
    if (boardChecker.checkBoardForWinner(board)) {
      state = "win";
      log.info("{} has won the game", passivePlayer.getPiece());

    } else {
      state = "draw";
      log.info("It is a draw. Game over");
    }
  }

  /**
   * This method requests a move from the active player
   */
  private void move()  {
    try{
      Position moveAttempt = activePlayer.makeMove(board);
      if (moveIsValid(moveAttempt, board)){

        board = new Board(board, moveAttempt, activePlayer.getPiece());

        log.info("{} placed a piece at {}", activePlayer.getPiece(), moveAttempt);
      }
    } catch (MoveException e){

    }
  }

  /**
   * This method checks if an attempted move is a valid move to make on the given board
   * @param moveAttempt the move to be attempted
   * @param board the current state of the board
   * @return boolean returns true if the move is within the boundaries of the board, and the position is currently vacant
   */
  public static boolean moveIsValid(Position moveAttempt, Board board){
    return board.positionWithinBoard(moveAttempt) && board.isPositionVacant(moveAttempt);
  }

  /**
   * This method swaps the turns between the active and passive players
   */
  private void swapTurns() {
    Player temp = activePlayer;
    activePlayer = passivePlayer;
    passivePlayer = temp;
  }

  /**
   * This method determines if no more moves can be made on the baord
   * @return boolean returns true if all positions on the board are taken
   */
  private boolean noMoreMoves() {
    for (int row = 0; row < board.getSize(); row++) {
      for (int col = 0; col < board.getSize(); col++) {
        if (board.isPositionVacant(new Position(row, col))) {
          return false;
        }
      }
    }
    return true;
  }


  /**
   * Gets the current state of the game
   * @return String state of the game
   */
  public String getState(){
    return state;
  }

  /**
   * Gets the player whose turn it currently is
   * @return Player the active player
   */
  public Player getActivePlayer(){
    return activePlayer;
  }

  /**
   * Returns the current state of the board
   * @return Board the current board
   */
  public Board getBoard(){
    return board;
  }

  /**
   * Abstract method to be extended defining how the board is to be displayed
   */
  public abstract void display();
}
