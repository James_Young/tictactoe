package com.thehutgroup.accelerator.components;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;

import java.util.*;

/**
 * <h1>Board Checker</h1>
 * Checks the board to see if there is  a winner
 */
public class BoardChecker {

  private int countToWin;
  private List<BoardLine> boardLines;

  /**
   * Constructor for the board checker, requiring the number of counters in a row for a player to win and the board from which to generate boarlines.
   * The board lines are used to iterate over positions on the board object
   * @param countToWin The number of counters in a row needed for a player to win the game
   * @param board The board object
   */
  public BoardChecker(int countToWin, Board board){
    this.countToWin = countToWin;
    boardLines = generateBoardLinesToCheck(board);
  }

  /**
   * Checks the board for a winner
   * @param board The current state of the board
   * @return boolean Returns true if a player has won by getting a number of their counters in a row
   */
  public boolean checkBoardForWinner(Board board){

    boardLines = generateBoardLinesToCheck(board);
    for (int i = 0; i < boardLines.size(); i++){
      if (lineIsWin(board, boardLines.get(i))){
        return true;
      }
    }
    return false;
  }

  /**
   * Resets the board lines to their initial starting points
   */
  private void resetBoardLines(){
    for (int i = 0; i < boardLines.size(); i ++){
      boardLines.get(i).reset();
    }
  }

  /**
   * Generates the board lines which are used to check for a winneer
   * @param board The current state of the board
   * @return List of Boardline List of board lines used to check horizontal, vertical and diagonal lines for a winner
   */
  private List<BoardLine> generateBoardLinesToCheck(Board board){

    List<BoardLine> boardLines = new ArrayList<>();

    MoveVector moveHorizontal = new MoveVector(0, 1);
    MoveVector moveVertical = new MoveVector(1, 0);
    MoveVector moveDiagonalUp = new MoveVector(1, 1);
    MoveVector moveDiagonalDown = new MoveVector(-1, 1);

    for (int row = 0; row < board.getSize(); row++){
      Position startPosition = new Position(row, 0);
      boardLines.add(new BoardLine(startPosition, moveHorizontal, board));
      boardLines.add(new BoardLine(startPosition, moveDiagonalUp, board));
      boardLines.add(new BoardLine(startPosition, moveDiagonalDown, board));
    }

    for (int col = 0; col < board.getSize(); col++){
      Position startPosition = new Position(0, col);
      boardLines.add(new BoardLine(startPosition, moveVertical, board));
      boardLines.add(new BoardLine(startPosition, moveDiagonalUp, board));
      boardLines.add(new BoardLine(new Position(board.getSize()-1, col), moveDiagonalDown, board));
    }

    return boardLines;
  }

  /**
   * Checks along the positions defined by an individual board line for winning number of counters in a row
   * @param board the current state of the board
   * @param boardLine the boardline being used to iterate over positions on the board
   * @return boolean Returns true if a player has a winning number of counters in a row
   */
  private boolean lineIsWin(Board board, BoardLine boardLine){
    Piece previousPieceType = board.getPiece(boardLine.getCurrentPosition());
    Piece currentPieceType;

    int runningCount = 1;
    while(boardLine.hasNext()){

      currentPieceType = board.getPiece(boardLine.next());

      if (currentPieceType == null){
        runningCount = 1;
        previousPieceType = currentPieceType;
        continue;
      }


      if (currentPieceType == previousPieceType){
        runningCount += 1;
        if (runningCount >= countToWin){
          return true;
        }
      }

    }
    return false;
  }
}
