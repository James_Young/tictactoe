package com.thehutgroup.accelerator.components;

/**
 * <h1>Move vector</h1>
 * Defining the amount by which
 */
public class MoveVector {
  private int col;
  private int row;
  public MoveVector(int row, int col){
    this.row = row;
    this.col = col;
  }

  public int getRow() {
    return row;
  }

  public int getCol() {
    return col;
  }
}
