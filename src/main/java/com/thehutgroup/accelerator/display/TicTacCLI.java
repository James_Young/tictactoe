package com.thehutgroup.accelerator.display;

import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.components.Piece;
import com.thehutgroup.accelerator.components.GameRunner;
import com.thehutgroup.accelerator.player.Player;

/**
 * Tic-Tac Command Line Interface
 * A command line interface to the game runner class. The main purpose of this class is to show the state of the board
 */
public class TicTacCLI extends GameRunner {

  /**
   * Constructor which passes the parameters to the underlying gamerunner class
   * @param player1 first player
   * @param player2 second player
   * @param size size of the tic-tac board, allowing for an 'n'-sized board
   */
  public TicTacCLI(Player player1, Player player2, int size) {
    super(player1, player2, size);
  }

  /**
   * Method to display the current state of the board in the console
   */
  @Override
  public void display() {


    System.out.println();
    System.out.print("  || ");
    for(int i = 0; i < getBoard().getSize(); i++){
      System.out.print(i+1 + " | ");
    }
    System.out.println();

    System.out.print("==||");
    for(int i = 0; i < getBoard().getSize(); i++){
      System.out.print("====");
    }
    System.out.println();

    for (int row = 0; row < getBoard().getSize(); row++) {
      System.out.print(row + 1 + " ||");
      for (int col = 0; col < getBoard().getSize(); col++) {
        Piece toDraw = getBoard().getPiece(new Position(row, col));
        if (toDraw == null) {
          System.out.print(" " + " " +" |");
        } else {
          System.out.print(" " + toDraw +" |");
        }
      }

      System.out.print("\n--||");
      for(int i = 0; i < getBoard().getSize(); i++){
        System.out.print("---|");
      }
      System.out.println();

    }
    System.out.println();
    switch (getState()) {
      case "play":
        System.out.println("It is " + getActivePlayer().getPiece() + "'s turn");
        break;
      case "win":
        System.out.println(getActivePlayer().getPiece() + " won!");
        break;
      case "draw":
        System.out.println("It's a draw!");
        break;
      default:
        break;
    }
  }
}
