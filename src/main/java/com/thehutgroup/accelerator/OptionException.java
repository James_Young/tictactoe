package com.thehutgroup.accelerator;

/**
 * <h1>Option Exception</h1>
 * For handling exceptions occuring when input is required from a user specifying a menu option
 */
public class OptionException extends Exception {

  /**
   * Handle the exception with a message
   * @param message summarizing why the exception was called
   */
  public OptionException(String message){
    super(message);
  }

  /**
   * Handle the exception with a message, and with the underlying cause of the exception
   * @param message summarizing why the exception was called
   * @param e the causing exception
   */
  public OptionException(String message, Exception e){
    super(message, e);
  }
}

