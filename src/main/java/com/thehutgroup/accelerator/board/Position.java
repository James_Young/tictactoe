package com.thehutgroup.accelerator.board;


import java.util.Objects;

/**
 * <h1>Position</h1>
 * Defines a position on a board object by the row and column
 */
public class Position {

    private int row;
    private int column;

    /**
     * Constructor for the position
     * @param row int
     * @param column int
     */
    public Position(int row, int column){
        this.row = row;
        this.column = column;
    }

    /**
     * Gets the row of the defined position
     * @return row int
     */
    public int getRow(){
        return row;
    }

    /**
     * Gets the column of the defined position
     * @return column int
     */
    public int getColumn(){
        return column;
    }

    /**
     * Defines how the position is displayed in a string format
     * @return String
     */
    @Override
    public String toString() {
        return "Position{" +
            "row=" + row +
            ", column=" + column +
            '}';
    }

    /**
     * Defines how two position objects are equivalent
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Position position = (Position) o;
        return row == position.row &&
            column == position.column;
    }

    /**
     * Defines how the hash code is generated from a position object
     * @return
     */
    @Override
    public int hashCode() {
        return Objects.hash(row, column);
    }
}
