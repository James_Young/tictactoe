package com.thehutgroup.accelerator.board;


import com.thehutgroup.accelerator.components.Piece;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Board</h1>
 * The tic-tac-toe board
 */
public class Board {

    private int boardSize;
    private Piece[][] board;

    /**
     * Alternative constructor for the board. This takes an existing board object, and a new move by a specified piece.
     * The new board is constructed with a counter placed at the new position
     * @param board
     * @param newMove
     * @param piece
     */
    public Board(Board board, Position newMove, Piece piece){
        this.board = board.getPieces();
        boardSize = board.getSize();
        board.setPiece(newMove, piece);
    }

    /**
     * Constructs a new board using the state of an existing board
     * @param board
     */
    public Board(Board board){
        this.board = board.getPieces();
        boardSize = board.getSize();
    }

    /**
     * Gets the array of pieces on the board
     * @return board Piece[][]
     */
    public Piece[][] getPieces(){
        return board;
    }

    /**
     * Constructor of a new board, given only the intended size of the board
     * @param boardSize int
     */
    public Board(int boardSize){
        this.boardSize = boardSize;
        board = new Piece[getSize()][getSize()];
    }

    /**
     * Gets the piece at the specified position
     * @param position position at which we wish to know the piece
     * @return Piece piece at the position
     */
    public Piece getPiece(Position position){
        return board[position.getRow()][position.getColumn()];
    }

    /**
     * places a counter at the specified position of the board
     * @param position
     * @param piece
     */
    private void setPiece(Position position, Piece piece){
        board[position.getRow()][position.getColumn()] = piece;
    }

    /**
     * Checks if the position is valid and vacant, and is therefore a valid move
     * @param position
     * @return boolean returns true if the position is vacant and within the boundaries of the board
     */
    public boolean isPositionVacant(Position position){
        return positionWithinBoard(position)
                && getPiece(position) == null;
    }

    /**
     * Gets a list of the available positions on the board
     * @return List of positions
     */
    public List<Position> getAvailablePositions(){
        List<Position> availablePositions = new ArrayList<>();
        for (int row=0; row < boardSize; row++){
            for (int col=0; col < boardSize; col++){
                Position position = new Position(col, row);
                if (isPositionVacant(position))
                    availablePositions.add(position);
            }
        }
        return availablePositions;
    }

    /**
     * Gets the size of the board
     * @return int boardSize
     */
    public int getSize(){
        return boardSize;
    }

    /**
     * Checks if the position is within the boundaries of the board
     * @param position
     * @return boolean Returns true if the position is within the boundaries of the board
     */
    public boolean positionWithinBoard(Position position){
        return 0 <= position.getRow() && position.getRow() < getSize()
                && 0 <= position.getColumn() && position.getColumn() < getSize();
    }

}