package com.thehutgroup.accelerator.player.strategy.impl;

import com.thehutgroup.accelerator.board.Board;
import com.thehutgroup.accelerator.board.Position;
import com.thehutgroup.accelerator.components.Piece;
import com.thehutgroup.accelerator.player.exceptions.MoveException;
import com.thehutgroup.accelerator.player.strategy.MoveStrategy;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class NormalMoveStrategyTest {
  @Test
  public void testMakeMoveOnEmptyBoard() throws MoveException {
    Board board = new Board(3);
    MoveStrategy moveStrategy = new EasyMoveStrategy();
    List<Position> availablePositions = board.getAvailablePositions();
    Position chosenPosition = moveStrategy.makeMove(board);
    assertTrue(availablePositions.contains(chosenPosition));
  }

  @Test
  public void testMakeMoveOnNearlyFullBoard() throws MoveException {
    Board board = new Board(2);
    board = new Board(board, new Position(0, 0 ), Piece.X);
    board = new Board(board, new Position(1, 0 ), Piece.O);
    board = new Board(board, new Position(0, 1 ), Piece.X);
    MoveStrategy moveStrategy = new EasyMoveStrategy();
    List<Position> availablePositions = board.getAvailablePositions();
    Position chosenPosition = moveStrategy.makeMove(board);
    assertTrue(availablePositions.contains(chosenPosition));
  }

  @Test(expected = MoveException.class)
  public void testMakeMoveOnFullBoard() throws MoveException {
    Board board = new Board(2);
    board = new Board(board, new Position(0, 0 ), Piece.X);
    board = new Board(board, new Position(1, 0 ), Piece.O);
    board = new Board(board, new Position(0, 1 ), Piece.X);
    board = new Board(board, new Position(1, 1 ), Piece.O);
    MoveStrategy moveStrategy = new EasyMoveStrategy();
    List<Position> availablePositions = board.getAvailablePositions();
    Position chosenPosition = moveStrategy.makeMove(board);
  }

}